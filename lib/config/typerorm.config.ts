import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import * as config from 'config';

const dbConfig = config.get('db');

export const typeOrmConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'postgres',
  password:'root',
  database: 'skalpeldb',
  entities: [__dirname + '/../entity/**/*.entity{.ts,.js}'],
  migrations : [ __dirname + '/../infrastructure/database/migrations/**/*.js'],
  synchronize: dbConfig.synchronize,
  cli : {
    entitiesDir  :__dirname + "/../entity/",
    migrationsDir:__dirname + "/../infrastructure/database/migration/"
}
};
