import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Users } from './users.entity';
import * as bcrypt from 'bcryptjs';
@Entity('passwords', { schema: 'public' })
export class Passwords extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  public id: number;

  @Column('timestamp', { nullable: false, name: 'user_uid' })
  public userUid: Date;

  @Column('timestamp', {
    nullable: false,
    default: () => 'now()',
    name: 'created_at',
  })
  public createdAt: Date;

  @Column('timestamp', { nullable: true, name: 'modified_at' })
  public modifiedAt: Date | null;

  @Column('varchar', { nullable: true, length: 255, name: 'password_md5' })
  public passwordMd5: string | null;

  @Column('varchar', { nullable: true, length: 255, name: 'salt' })
  public salt: string | null;

  @OneToOne((type) => Users, (users) => users.password, {})
  @JoinColumn({ name: 'user_uid' })
  public user: Users;

  async validatePassword(password: string): Promise<boolean> {
    const hash = await bcrypt.hash(password, this.salt);
    return hash === this.passwordMd5;
  }
}
