import {
  BaseEntity,
  BeforeInsert,
  Column,
  Entity,
  PrimaryColumn,
} from 'typeorm';
import { v4 as uuidv4 } from 'uuid';

@Entity('email_verification', { schema: 'public' })
export class EmailVerification extends BaseEntity {
  @PrimaryColumn({ type: 'varchar', name: 'uid' }) public uid: string;
  @Column({ type: 'varchar', name: 'uid_user' }) public userUid: string;
  @Column('timestamp', { name: 'verification_date' })
  public emailVerificationDate: Date;
  @Column('varchar', { length: 100, name: 'email' }) public email:
    | string
    | null;
  @BeforeInsert() public uuid() {
    this.uid = uuidv4();
  }
}
