import {
    BaseEntity,
  BeforeUpdate,
    Column,
    Entity,
    JoinColumn,
    OneToOne,
    PrimaryGeneratedColumn,
  } from 'typeorm';

import { Blobs } from './blob.entity';
  
  @Entity('articles', { schema: 'public' })
  export class Articles extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
    public id: number;
  
    @Column('varchar', { unique: true, length: 200, name: 'title' })
    public title: string | null;
  
    @Column('varchar', { length: 2000, name: 'content' })
    public content: string | null;

    @Column('varchar', {  name: 'user_uid' })
    public userUid: string | null;

    @Column('varchar', { length: 100, name: 'status' })
    public status: string | null;

    @Column('integer', { name: 'blob_id' })
    public blobId: number | null;
  
    @Column('integer', { name: 'artycle_type' })
    public type: number | null;
  
    @Column('timestamp', {
      nullable: false,
      default: () => 'now()',
      name: 'created_at',
    })
    public createdAt: Date;

    @Column('timestamp', { nullable: true, name: 'modified_at' })
    public modifiedAt: Date | null;
  
    @OneToOne((type) => Blobs, (blob) => blob.id, {})
    @JoinColumn({ name: "blob_id"})
    public blobs: Blobs;

    @BeforeUpdate() public modified() {
      this.modifiedAt = new Date();
     
  }
  
  
   
  }
  