import {
  BaseEntity,
  BeforeInsert,
  Column,
  Entity,
  OneToOne,
  PrimaryColumn,
} from 'typeorm';

import { v4 as uuidv4 } from 'uuid';
import { Passwords } from './passwords.entity';

@Entity('users', { schema: 'public' })
export class Users extends BaseEntity {
  @PrimaryColumn({ type: 'varchar', name: 'uid' })
  public uid: string;

  @Column('varchar', { unique: true, length: 100, name: 'login' })
  public login: string | null;

  @Column('varchar', { length: 100, name: 'email' })
  public email: string | null;

  @Column('varchar', { length: 36, name: 'status' })
  public status: string | null;

  @Column('integer', { name: 'password_id' })
  public passwordId: number | null;

  @Column('varchar', { length: 36, name: 'account_type' })
  public accountType: string | null;

  @Column('timestamp', {
    nullable: false,
    default: () => 'now()',
    name: 'created_at',
  })
  public createdAt: Date;

  @Column('timestamp', { nullable: true, name: 'modified_at' })
  public modifiedAt: Date | null;

  @OneToOne((type) => Passwords, (passwords) => passwords.user, {})
  public password: Passwords;

  public validatePassword(password) {
    return true;
  }

  @BeforeInsert() public uuid() {
    this.uid = uuidv4();
  }
}
