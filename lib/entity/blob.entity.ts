import { BaseEntity, Column
    , Entity
    , PrimaryGeneratedColumn
    } from "typeorm";

@Entity("blobs", {schema: "public" } )
export class Blobs extends BaseEntity {
    @PrimaryGeneratedColumn({ type: "int", name: "id" }) public id: number | null;
    @Column("timestamp with time zone", {nullable: false, default: () => "now()", name: "created_at",  }) public createdAt: Date;
    @Column("varchar",{ name: "name"})         public  name: string | null;
    @Column("varchar",{ name: "file_type"})    public  fileType: string | null;
    @Column("int",    { name: "size_of_file"}) public  sizeOfFile: number | null;
    @Column("bytea",  { name: "file" })        public  file: Buffer| null;
}
