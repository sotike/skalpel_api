import { ConflictException } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { ChangePasswordCredentialsDto } from '../../src/auth/dto/auth-credentials.dto';
import * as bcrypt from 'bcryptjs';
import { Users } from 'lib/entity/users.entity';
import { Passwords } from 'lib/entity/passwords.entity';

@EntityRepository(Passwords)
export class PasswordRepository extends Repository<Passwords> {
  async resetPassword(
    user: Users,
    resetPasswordCredentialsDto: ChangePasswordCredentialsDto,
  ): Promise<Passwords> {
    const { oldPassword, newPassword } = resetPasswordCredentialsDto;
    const password: Passwords = await this.findOne({ userUid: user.uid });
    if (oldPassword === newPassword) {
      throw new ConflictException('should be different');
    }
    if (
      password &&
      password.passwordMd5 !==
        (await this.hashPassword(oldPassword, password.salt))
    ) {
      throw new ConflictException('The old password is not correct');
    }
    const salt = await bcrypt.genSalt();
    password.salt = salt;
    password.passwordMd5 = await this.hashPassword(newPassword, salt);
    await password.save();
    return password;
  }

  async hashPassword(password: string, salt: string) {
    return bcrypt.hash(password, salt);
  }
}
