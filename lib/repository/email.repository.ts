import { EntityRepository, Repository } from 'typeorm';
import { Passwords } from 'lib/entity/passwords.entity';
import { EmailVerification } from 'lib/entity/emailVerification.entity';

@EntityRepository(EmailVerification)
export class EmailRepository extends Repository<EmailVerification> {
  async getEmailByUid(uid: string): Promise<EmailVerification> {
    const emailVerification = await this.findOne({ uid });
    return await this.findOne({ uid: uid });
  }
}
