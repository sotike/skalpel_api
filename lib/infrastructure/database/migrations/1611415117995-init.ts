import { MigrationInterface, QueryRunner } from 'typeorm';

export class init1611415117995 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
        CREATE TABLE users (
          uid  uuid default uuid_generate_v1mc()  NOT NULL,
          login varchar(100) unique,
          password_id int,
          email varchar(100) unique,
          account_type varchar(36),
          status varchar(36),
          email_verification_date timestamp with time zone null,
          created_at timestamp with time zone not null default now(),
          modified_at timestamp with time zone null,
          CONSTRAINT users_pkey PRIMARY KEY (uid)
        );
        CREATE TABLE passwords (
          id serial  NOT NULL,
          password_md5 varchar(255) NULL,
          user_uid uuid,
          created_at timestamp with time zone not null default now(),
          modified_at timestamp with time zone null,
          salt varchar(100),
          status_id int,
          CONSTRAINT passwords_pkey PRIMARY KEY (id)
        );
        create table email_verification(
          uid uuid default uuid_generate_v1mc()  NOT NULL,
          uid_user uuid,
          verification_date timestamp with time zone null,
          email varchar(100)
          );
          ALTER TABLE users ADD FOREIGN KEY (password_id) REFERENCES passwords (id);
          ALTER TABLE passwords ADD FOREIGN KEY (user_uid) REFERENCES users (uid);
          ALTER TABLE email_verification ADD FOREIGN KEY (uid_user) REFERENCES users (uid);
         
        `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
