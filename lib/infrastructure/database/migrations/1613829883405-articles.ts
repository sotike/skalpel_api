import {MigrationInterface, QueryRunner} from "typeorm";
export class articles1613829883405 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE articles (
            id serial,
            user_uid uuid,
            title  varchar(500),
            content varchar (10000),
            blob_id   int,
            artycle_type int,
            status varchar(36),
            created_at timestamp with time zone not null default now(),
            modified_at timestamp with time zone null,
            CONSTRAINT articles_pkey PRIMARY KEY (id)
          );
         CREATE TABLE blobs (
              id serial 
             ,file  bytea
             ,file_type varchar(10) null
             ,size_of_file int
             ,name varchar(50) null
             ,created_at timestamp with time zone not null default now()
             ,constraint blobs_pkey PRIMARY KEY (id)
            );
            `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
