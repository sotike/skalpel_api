import { ConflictException } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { SignInCredentialsDto } from '../../src/auth/dto/auth-credentials.dto';
import { Users } from 'lib/entity/users.entity';
@EntityRepository(Users)
export class UserRepository extends Repository<Users> {
  async validateUserPassword(
    signInCredentialsDto: SignInCredentialsDto,
  ): Promise<Users> {
    const { login, password } = signInCredentialsDto;
    const userWithPassword = await this.createQueryBuilder('users')
      .leftJoinAndSelect('users.password', 'password')
      .where('users.login = :login', { login })
      .getOne();
    if (
      userWithPassword &&
      (await userWithPassword.password.validatePassword(password))
    ) {
      return userWithPassword;
    } else {
      throw new ConflictException('Login is incorrect');
    }
  }

  async getUserByEmail(email: string): Promise<Users> {
    return await this.findOne({ email });
  }

  async getUserByUid(uid: string): Promise<Users> {
    return this.findOne(uid);
  }

  async getUserWithPasswordByUserUid(uid: string) {
    return await this.createQueryBuilder('users')
      .leftJoinAndSelect('users.password', 'password')
      .where('users.uid = :userUid', { userUid: uid })
      .getOne();
  }
}
