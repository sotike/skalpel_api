import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { NestExpressApplication } from '@nestjs/platform-express';
import { join } from 'path';
import { Logger } from '@nestjs/common';
import * as config from 'config';
async function bootstrap() {
  const serverConfig = config.get('servers');
  const logger = new Logger('bootstrap'); // verbose debag, error ,warn
  const port = process.env.PORT || serverConfig.port;
  const app = await NestFactory.create<NestExpressApplication>(
    AppModule,
  );
 
  if (process.env.NODE_ENV ==='dev'){
    app.enableCors();
  }
  
  else {
    app.enableCors({origin: serverConfig.origin});
    Logger.log("Accepting requests from origin : ", serverConfig.origin);
  }
   
  app.useStaticAssets(join(__dirname ));
  await app.listen(port);
  logger.log(`Application listening on port ${port}`);
}
bootstrap();
