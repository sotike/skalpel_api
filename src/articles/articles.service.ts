import { ConflictException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Articles } from 'lib/entity/articles.entity';
import { Blobs } from 'lib/entity/blob.entity';
import { UserRepository } from 'src/users/user.repository';
import { ArticlesRepository } from './articles.repository';
import { ArticlesCredentialsDto, ArticlesDeleteCredentialsDto
     , ArticlesUpdateCredentialsDto } from './dto/articles-credentials.dto';
import fs = require("fs");
import { Users } from 'lib/entity/users.entity';
import { getConnection } from 'typeorm';
import { IArticlesDetails } from './types/articlesResponse';
import { detailsDateToTimeString } from './commonTool';
var path = require('path');

@Injectable()
export class ArticlesService {
constructor( 
     @InjectRepository(UserRepository)  private UserRepository: UserRepository
     , private ArticlesRepository : ArticlesRepository
){}

async articlesAdd(users, files, articlesCredentialsDto : ArticlesCredentialsDto): Promise<Articles> { 
     const imgBlob  = await  this.saveImgToBlob(files);
     const articles = await this.ArticlesRepository.addArticles(users,articlesCredentialsDto,imgBlob.id)
return articles;
}

async articlesUpdate(user : Users, files, articlesCredentialsDto : ArticlesUpdateCredentialsDto,req : Request): Promise<Articles> { 
     const imgBlob  = await  this.saveImgToBlob(files);
     const article = await this.ArticlesRepository.findOne({where : {id:articlesCredentialsDto.articleId}});
     if (!article){
          throw new ConflictException('Error article id! ');
     }
    if (!this.goodPermission(article,user)){
     throw new ConflictException('Not permission! ');
    }
     await this.deleteArticleBlob(article);
     await this.makeAvatarLink(articlesCredentialsDto.articleId, req)
     const newArticle = await this.ArticlesRepository.updateArticles(articlesCredentialsDto,imgBlob.id)
return newArticle;
}

async getArcticlesDetails(user : Users, req : Request): Promise<IArticlesDetails[]> { 
     const articles = await this.ArticlesRepository.getArticles();
     console.log(await this.makeAvatarLink(articles[0].blobId,req));
     const finalResponse : IArticlesDetails[] = [] as IArticlesDetails[];
     let oneEntity : IArticlesDetails = {} as IArticlesDetails;
     for ( const item of articles) {
          oneEntity.content = item.content;
          oneEntity.title   = item.title;
          oneEntity.type    = item.type;
          oneEntity.id      = item.id;
          oneEntity.date    = detailsDateToTimeString(item.createdAt);
          oneEntity.imageLink = await this.makeAvatarLink(item.blobId,req);
          finalResponse.push(oneEntity);
          oneEntity = {} as IArticlesDetails;
     }
     return finalResponse;
}    

async deleteArticle(user,  articleCredentialsDto: ArticlesDeleteCredentialsDto): Promise<number> { 
     const {id } =  articleCredentialsDto;
     const article = await this.ArticlesRepository.findOne({where : {id}});
     if (!article){
          throw new ConflictException('Error article id ! ');
     }
     if (!this.goodPermission(article,user)){
          throw new ConflictException('Not permission! ');
     }
    await this.deleteArticleBlob(article);
     const affected = await this.ArticlesRepository.deleteArticles(id);
     return affected;
}

private async deleteArticleBlob(article: Articles) {
     if (article){
          if (article.blobId) await this.ArticlesRepository.deleteArticleBlob(article.blobId);
          var filePath = "./dist/src/uploads/"+article.blobId+".png"; 
          if (await fs.existsSync(filePath)){
               fs.unlinkSync(filePath);
          } 
     }
}

private async saveImgToBlob(files): Promise<Blobs>{
     const imgBlob      = new Blobs;
     imgBlob.sizeOfFile = files.avatar[0].size;
     imgBlob.fileType   = files.avatar[0].mimetype;
     imgBlob.file       = files.avatar[0].buffer;
     imgBlob.name       = files.avatar[0].originalname;
     await imgBlob.save();
     return imgBlob;
}

 private async  makeAvatarLink(blobId: number, req: any = null) {
     return new Promise<string>(async (resolve, reject) => {
     try {
         const ext = ".png";
      if (await !fs.existsSync("./dist/src/uploads")){
          fs.mkdirSync("./dist/src/uploads");
     }  
         const pathToUpload =  "uploads/" + blobId + ext;
         const fileNameParam =  "./dist/src/uploads/" + blobId + ext;
         let url = (req) ? `${req.get("host")}/${pathToUpload}` :  fileNameParam;
         if (!url.includes("http") && !url.includes("localhost")) { url = "https://" + url; } else if (!url.includes("http")) { url = "http://" + url; } 
         if (fs.existsSync(fileNameParam)) {
           resolve(url);
         } else {
             const sqlFile = await getConnection()
                         .createQueryBuilder(Blobs, "blobsFiles")
                         .where("blobsFiles.id = :idFiltr", { idFiltr: blobId  }).getOne() as Blobs;
             if (!sqlFile) { resolve (null); }
             else {
                 const fileName = fileNameParam;
                 const targetPath = path.join(fileName);
                 fs.writeFileSync(targetPath, sqlFile.file);
                 resolve(url);
             }
         }
     } catch (error) {
          reject(error);
         }
     });
   }

   private goodPermission(article : Articles, users : Users){
        console.log("here i am : ");
        console.log(users);
        console.log("article: ");
        console.log(article);
     if (article.userUid !== users.uid && users.accountType !== "admin"){
        return false;
     }
     else{
          return true;
     }
}


}
