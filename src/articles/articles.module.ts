import { Module } from '@nestjs/common';
import { ArticlesController } from './articles.controller';
import { ArticlesService } from './articles.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from 'src/users/user.repository';
import { ArticlesRepository } from './articles.repository';
import { AuthModule } from 'src/auth/auth.module';

@Module({

 imports : [
  AuthModule,
  TypeOrmModule.forFeature([
    UserRepository,
    ArticlesRepository
  ]),
 ],

  controllers: [ArticlesController],
  providers: [ArticlesService]
})
export class ArticlesModule {}
