import { ConflictException, Delete, Get, Put, Req, UploadedFiles, UseGuards, UseInterceptors } from '@nestjs/common';
import { Body, Controller, Post, ValidationPipe } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { Users } from 'lib/entity/users.entity';
import { GetUser } from 'src/auth/get-user.decorator';
import { ArticlesService } from './articles.service';
import { ArticlesCredentialsDto, ArticlesDeleteCredentialsDto, ArticlesUpdateCredentialsDto } from './dto/articles-credentials.dto';

@Controller('articles')
@UseGuards(AuthGuard())
export class ArticlesController {
    constructor(private articlesSerwise: ArticlesService) {}

@Post('/')
@UseInterceptors(FileFieldsInterceptor([
  { name: 'avatar', maxCount: 1 }
]))
async addArticles(
  @GetUser() user: Users
  ,@UploadedFiles() files
  ,@Body(ValidationPipe) authCredentialsDto: ArticlesCredentialsDto
  ) : Promise<{ message: string }> {
    const message = await this.articlesSerwise.articlesAdd(user,files,authCredentialsDto);
    console.log(message);
    if(message){
      return { message : "The new artycle was successfully added" };
    }
    else {
      throw new ConflictException('Something wrong');
    }
}
@Get('/')
async articlesDetails(
  @GetUser() user: Users
  ,@Req() req
  ){
    const articles =  this.articlesSerwise.getArcticlesDetails(user,req);
    return articles;
}

@Delete('/')
async deleteArticles (
  @GetUser() user: Users
  ,@Body(ValidationPipe) articleCredentialsDto: ArticlesDeleteCredentialsDto
  ): Promise<{message: string}>{
    const affected =  await this.articlesSerwise.deleteArticle(user, articleCredentialsDto);
    if (affected === 0 ){
      throw new ConflictException(`The article with id = ${articleCredentialsDto.id} doesnt' exist`)
    }
    return {message : "the article was successfully deleted"};
}

@Put('/')
@UseInterceptors(FileFieldsInterceptor([
  { name: 'avatar', maxCount: 1 }
]))
async updateArticles(
  @GetUser() user: Users
  ,@UploadedFiles() files
  ,@Body(ValidationPipe) authCredentialsDto: ArticlesUpdateCredentialsDto
  ,@Req() req
  ) : Promise<{ message: string }> {
    const message = await this.articlesSerwise.articlesUpdate(user,files,authCredentialsDto,req);
    if(message){
      return { message : "The new artycle was successfully added" };
    }
    else {
      throw new ConflictException('Something wrong');
    }
}




}
