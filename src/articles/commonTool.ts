import moment = require("moment");

export function detailsDateToTimeString(date: Date): string {
    if (date === null) { return "n/a"; }
    if (date === undefined) { return "n/a"; }
    return moment(date).format("YYYY/MM/DD HH:mm")
}