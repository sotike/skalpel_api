export interface IArticlesDetails{
    id : number;
    title : string;
    content : string;
    type : number;
    date : string;
    imageLink : string;
}