import { EntityRepository, getConnection, Repository } from 'typeorm';
import { Articles } from 'lib/entity/articles.entity';
import { ArticlesCredentialsDto, ArticlesUpdateCredentialsDto } from './dto/articles-credentials.dto';
import { ConflictException } from '@nestjs/common';
import { Blobs } from 'lib/entity/blob.entity';
import { Users } from 'lib/entity/users.entity';
@EntityRepository(Articles)
export class ArticlesRepository extends Repository<Articles> {

    async addArticles(users : Users,articlesCredentialsDto : ArticlesCredentialsDto, imgId : number): Promise<Articles> {
          const {title, content, artycleType } = articlesCredentialsDto
            const articles = new  Articles()
            articles.userUid = users.uid;
            articles.blobId = imgId;
            articles.content = content;
            articles.title = title;
            articles.type = artycleType;
            await articles.save();
            return articles;
      }

  async getArticles(): Promise<Articles[]> {
    return await getConnection().manager.createQueryBuilder(Articles,"articles").orderBy("articles.id","ASC").getMany();
  }

  async updateArticles(articlesCredentialsDto : ArticlesUpdateCredentialsDto, imgId : number): Promise<Articles> {
    const {title, content, artycleType, articleId } = articlesCredentialsDto
    const articles   = await this.findOne({where : {id : articleId}});
    if (!articles)   throw new ConflictException(`Article with id = ${articleId} doesn't exist`);
    articles.title   = title;
    articles.content = content;
    articles.blobId  = imgId;
    await articles.save();
    return articles;
  }


  async deleteArticles(id: number){ 
    const result =  await getConnection()
    .createQueryBuilder()
    .delete()
    .from(Articles)
    .where("id = :id", { id: id })
    .execute();
    return result.affected ;

  }


  async deleteArticleBlob(id: number){ 
    const result =  await getConnection()
    .createQueryBuilder()
    .delete()
    .from(Blobs)
    .where("id = :id", { id: id })
    .execute();
    return result.affected ;

  }

}
