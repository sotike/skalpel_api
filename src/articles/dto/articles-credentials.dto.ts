import {
    IsString,
    IsUUID,
    Max,
    MaxLength,
    Min,
    min,
    MinLength,
  } from 'class-validator';
  
  export class ArticlesCredentialsDto {
    @IsString()
    @MinLength(4)
    @MaxLength(1000)
    title: string;
  
    @IsString()
    @MinLength(1)
    @MaxLength(10000)
    content: string;
  
    @IsString()
    @IsUUID()
    userUid: string;

    @IsString()
    artycleType: number;
  }
  

  export class ArticlesUpdateCredentialsDto{
    @IsString()
    @MinLength(4)
    @MaxLength(1000)
    title: string;
  
    @IsString()
    @MinLength(1)
    @MaxLength(10000)
    content: string;
  
    @IsString()
    userUid: string;

    @IsString()
    artycleType: number;
  
    @IsString()
    articleId: number;
  }


  export class ArticlesDeleteCredentialsDto{
    @IsString()
    id : number
  }