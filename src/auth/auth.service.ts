import {
  ConflictException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { Passwords } from 'lib/entity/passwords.entity';
import { Users } from 'lib/entity/users.entity';
import {
  AuthCredentialsDto,
  ChangePasswordCredentialsDto,
  ResetPasswordCredentialsDto,
  SendEmailToResetPasswordCredentialsDto,
  SignInCredentialsDto,
} from './dto/auth-credentials.dto';
import { emailVerification } from './email/emailCommon';
import * as bcrypt from 'bcryptjs';
import { JwtPayload } from './jwt-payload.interface';
import { PasswordRepository } from 'lib/repository/password.repository';
import { EmailRepository } from 'lib/repository/email.repository';
import { UserRepository } from 'src/users/user.repository';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserRepository)
    private UserRepository: UserRepository,
    private PasswordRepository: PasswordRepository,
    private EmailRepository: EmailRepository,
    private JwtService: JwtService,
  ) {}

  async signUp(authCredentialDto: AuthCredentialsDto): Promise<string> {
    const { login, password, email } = authCredentialDto;
    const userEntity = new Users();
    userEntity.login = login;
    userEntity.email = email;
    userEntity.accountType = 'user';
    userEntity.status = 'verification';
    try {
      await userEntity.save();
    } catch (error) {
      if (error.code === '23505') {
        throw new ConflictException('login or email already exists');
      }
    }
    await userEntity.save();
    const passwordEntity = new Passwords();
    passwordEntity.salt = await bcrypt.genSalt();
    passwordEntity.passwordMd5 = await this.PasswordRepository.hashPassword(
      password,
      passwordEntity.salt,
    );
    passwordEntity.user = userEntity;
    await passwordEntity.save();
    userEntity.passwordId = passwordEntity.id;
    await userEntity.save();

    const verificationUid = await emailVerification(
      userEntity.email,
      userEntity.uid,
    );
    console.log('email verification email: ' + verificationUid);
    return 'Registration is completed. Select the link in the email to activate your account.';
  }

  async signIn(signInCredentialsDto: SignInCredentialsDto) {
    const user = await this.UserRepository.validateUserPassword(
      signInCredentialsDto,
    );
    if (!user) {
      throw new UnauthorizedException('Invalid credentials');
    }
    if (user.status === 'verification') {
      throw new UnauthorizedException(
        'User is inactive. Please varified Your email',
      );
    }
    const payload: JwtPayload = { login: user.login };
    const accessToken = await this.JwtService.sign(payload);
    return { accessToken, login : user.login, email: user.email };
  }

  async changePassword(
    user: Users,
    resetPasswordCredentialsDto: ChangePasswordCredentialsDto,
  ) {
    return this.PasswordRepository.resetPassword(
      user,
      resetPasswordCredentialsDto,
    );
  }

  async sendEmailToResetPassword(
    resetPasswordCredentialsDto: SendEmailToResetPasswordCredentialsDto,
  ) {
    const { email } = resetPasswordCredentialsDto;
    const user = await this.UserRepository.getUserByEmail(email);
    if (!user) {
      throw new ConflictException('Account does not exist');
    }
    return emailVerification(email, user.uid, false);
  }

  async resetPassword(
    resetPasswordCredentialsDto: ResetPasswordCredentialsDto,
  ): Promise<Passwords> {
    const { verificationUid, newPassword } = resetPasswordCredentialsDto;

    const email = await this.EmailRepository.getEmailByUid(verificationUid);
    if (email.emailVerificationDate)
      throw new ConflictException(
        'The password for the account was already changed ',
      );
    email.emailVerificationDate = new Date();
    await email.save();
    const users = await this.UserRepository.getUserWithPasswordByUserUid(
      email.userUid,
    );
    const password = users.password;
    password.salt = await bcrypt.genSalt();
    password.passwordMd5 = await this.PasswordRepository.hashPassword(
      newPassword,
      password.salt,
    );
    await password.save();
    return password;
  }

  async activate(activationUid: string): Promise<Users> {
    const email = await this.EmailRepository.getEmailByUid(activationUid);
    if (!email) throw new ConflictException('Activation Error');
    email.emailVerificationDate = new Date();
    await email.save();

    const users = await this.UserRepository.getUserWithPasswordByUserUid(
      email.userUid,
    );
    users.status = 'active';
    console.log(users);
    await users.save();
    return users;
  }
}
