import { UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { InjectRepository } from '@nestjs/typeorm';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { JwtPayload } from './jwt-payload.interface';


import * as config from 'config';
import { Users } from 'lib/entity/users.entity';
import { UserRepository } from 'src/users/user.repository';

const JwtConfig = config.get('jwt');

export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
    
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: process.env.JWT_SECRET || JwtConfig.secret,
    });

    console.log("hello : ");
  }

  async validate(payload: JwtPayload): Promise<Users> {
    console.log("hello passport strategy...");

    const { login } = payload;
    const user = await this.userRepository.findOne({ login });
    console.log("validate: " ) ;
    console.log(user);

    if (!user) {
      console.log("aksdfjkasdjf;a");
      throw new UnauthorizedException();
    }
    return user;
  }
}
