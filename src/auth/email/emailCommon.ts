import * as async from 'async';
import { EmailVerification } from 'lib/entity/emailVerification.entity';
import nodemailer = require('nodemailer');
import { IEmailOptions } from './types/emailOptions';

export class SendNotification {
  public async sendNotification(mailOptions: any) {
    const myTransporter = this.getTransporter();
    if (myTransporter === null) {
      console.error('Message not sent.');
      return;
    }
    async.parallel(
      {
        out: (callback) =>
          myTransporter
            .sendMail(mailOptions)
            .then((out) => callback(out))
            .catch((error) => callback(error, null)),
      },
      async (error, results: { out: any }) => {
        if (!error) {
          console.log('Notificaton has been send: ' + results.out.subject);
        } else console.log('Notificaton has been send: ' + error);
      },
    );
  }

  private getTransporter() {
    const transporter = nodemailer.createTransport({
      host: process.env.EMAILS_SEND_SMTP_SERVER || 'smtp.gmail.com',
      port: process.env.EMAILS_SEND_SMTP_PORT || '465',
      secure: true, // true for 465, false for other ports
      auth: {
        user:
          process.env.EMAILS_SEND_ACCOUNT ||
          'poruzumienie.chirurgow.skalpel@gmail.com', // generated ethereal user
        pass:
          process.env.EMAILS_SEND_PASSWORD || 'poruzumienieChirurgowSkalpel', // generated ethereal password
      },
    });
    return transporter;
  }
}

export function getEmailFrom(): string {
  if (process.env.EMAILS_SYSTEM_NAME) {
    return (
      process.env.EMAILS_SYSTEM_NAME + ' <' + process.env.EMAIL_ACCOUNT + '>'
    );
  }
  return 'Wager Match Test <wagermatchtest@gmail.com>';
}

export function shellSend(): boolean {
  if (process.env.EMAILS_SEND) {
    if (process.env.EMAILS_SEND.toLowerCase().trim() === 'yes') {
      return true;
    }
  }
  return false;
}

export function sendEmail(mailOptions: any) {
  if (shellSend()) {
    new SendNotification().sendNotification(mailOptions);
  }
}

export function setEmailOption(
  from,
  to,
  cc,
  subject,
  text,
  html,
): IEmailOptions {
  const emailOptions: IEmailOptions = { from, to, cc, subject, text, html };
  return emailOptions;
}

export async function emailVerification(
  email: string,
  userUid: string,
  isResetPassword = false,
): Promise<string> {
  const emailEntity = new EmailVerification();
  emailEntity.email = email;
  emailEntity.userUid = userUid;
  await emailEntity.save();
  const link = isResetPassword
    ? process.env.RESET_PASSWORD_FRON_END_LINK
    : process.env.ACTIVATION_LINK;

  const finalLink = isResetPassword
    ? link + '/' + emailEntity.uid + '?isReset = true'
    : link + '/' + emailEntity.uid;

  const mailOptions = setEmailOption(
    process.env.EMAILS_FROM,
    email,
    process.env.EMAILS_FROM,
    'Email verification',
    'verification',
    `Activation link : <a href = '${finalLink}'> Press me</a>`,
  );

  new SendNotification().sendNotification(mailOptions);
  return emailEntity.uid;
}
