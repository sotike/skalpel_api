import {
  Body,
  ConflictException,
  Controller,
  Get,
  Logger,
  Param,
  Patch,
  Post,
  Res,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import {
  AuthCredentialsDto,
  ChangePasswordCredentialsDto,
  ResetPasswordCredentialsDto,
  SendEmailToResetPasswordCredentialsDto,
  SignInCredentialsDto,
} from './dto/auth-credentials.dto';
import { GetUser } from './get-user.decorator';
import { Users } from 'lib/entity/users.entity';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/signup')
  async signUp(
    @Body(ValidationPipe) authCredentialsDto: AuthCredentialsDto,
  ): Promise<{ message: string }> {
    const message = await this.authService.signUp(authCredentialsDto);
    return { message };
  }

  @Post('/signin')
  async signIn(
    @Body(ValidationPipe) signInCredentialsDto: SignInCredentialsDto,
  ): Promise<{ accessToken: string }> {
    return await this.authService.signIn(signInCredentialsDto);
  }

  @Patch('/change-password')
  @UseGuards(AuthGuard())
  async changePassword(
    @GetUser() user: Users,
    @Body(ValidationPipe)
    resetPasswordCredentialsDto: ChangePasswordCredentialsDto,
  ) {
    return await this.authService.changePassword(
      user,
      resetPasswordCredentialsDto,
    );
  }

  @Post('/reset-password')
  async sendEmailToResetPassword(
    @Body(ValidationPipe)
    resetPasswordCredentialsDto: SendEmailToResetPasswordCredentialsDto,
  ): Promise<{ message: string }> {
    Logger.debug(
      'verificationUid: ' +
        (await this.authService.sendEmailToResetPassword(
          resetPasswordCredentialsDto,
        )),
    );
    return { message: 'Email has been sent' };
  }

  @Patch('/reset-password')
  async resetPassword(
    @Body(ValidationPipe)
    resetPasswordCredentialsDto: ResetPasswordCredentialsDto,
  ): Promise<{ message: string }> {
    if (await this.authService.resetPassword(resetPasswordCredentialsDto)) {
      return { message: 'successfully reset password' };
    } else {
      throw new ConflictException('Something wrong');
    }
  }

  @Get('/activation/:uid')
  async activate(@Param('uid') uid: string, @Res() res) {
    await this.authService.activate(uid);
    res.writeHead(301, { Location: process.env.FRONTEND_LINK });
    res.end();
  }
}
