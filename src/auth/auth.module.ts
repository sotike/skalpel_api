import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';

import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './jwt.strategy';
import { SendNotification } from './email/emailCommon';
import * as config from 'config';
import { PasswordRepository } from 'lib/repository/password.repository';
import { EmailRepository } from 'lib/repository/email.repository';
import { UserRepository } from 'src/users/user.repository';

const JwtConfig = config.get('jwt');

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: process.env.JWT_SECRET || JwtConfig.secret,
      signOptions: {
        expiresIn: JwtConfig.expiresIn,
      },
    }),
    TypeOrmModule.forFeature([
      UserRepository,
      PasswordRepository,
      EmailRepository,
    ]),
    SendNotification,
  ],
  exports: [JwtStrategy, PassportModule],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy, SendNotification],
})
export class AuthModule {}
