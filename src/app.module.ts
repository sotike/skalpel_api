import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from '@nestjs/config';
import { typeOrmConfig } from 'lib/config/typerorm.config';
import { UsersModule } from './users/users.module';
import { ArticlesModule } from './articles/articles.module';


@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
      isGlobal: true,
      ignoreEnvFile: false,
    }),
   
     TypeOrmModule.forRoot(typeOrmConfig),
     AuthModule,
     UsersModule,
     ArticlesModule,
  ],
})
export class AppModule {}
